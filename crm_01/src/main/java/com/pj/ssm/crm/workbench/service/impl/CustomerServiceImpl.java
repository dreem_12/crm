package com.pj.ssm.crm.workbench.service.impl;

import com.pj.ssm.crm.commons.utils.Contants;
import com.pj.ssm.crm.commons.utils.DateFormatUtils;
import com.pj.ssm.crm.commons.utils.UUIDUtils;
import com.pj.ssm.crm.settings.domain.User;
import com.pj.ssm.crm.workbench.domain.Customer;
import com.pj.ssm.crm.workbench.mapper.ContactsActivityRelationMapper;
import com.pj.ssm.crm.workbench.mapper.ContactsMapper;
import com.pj.ssm.crm.workbench.mapper.ContactsRemarkMapper;
import com.pj.ssm.crm.workbench.mapper.CustomerContactsRelationMapper;
import com.pj.ssm.crm.workbench.mapper.CustomerMapper;
import com.pj.ssm.crm.workbench.mapper.CustomerRemarkMapper;
import com.pj.ssm.crm.workbench.mapper.TranMapper;
import com.pj.ssm.crm.workbench.service.CustomerService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * @Description: 此处添加描述 //TODO
 * @Author: Dreem
 * @Date: 2022 05 15 11:10
 **/
@Service("customerService")
public class CustomerServiceImpl implements CustomerService {

    @Resource
    private CustomerMapper customerMapper;

    @Resource
    private ContactsMapper contactsMapper;

    @Resource
    private TranMapper tranMapper;

    @Resource
    private CustomerRemarkMapper customerRemarkMapper;

    @Resource
    private ContactsRemarkMapper contactsRemarkMapper;

    @Resource
    private CustomerContactsRelationMapper customerContactsRelationMapper;

    @Resource
    private ContactsActivityRelationMapper contactsActivityRelationMapper;

    @Override
    public List<Customer> queryAllCustomers(Customer conditionCustomer) {
        return customerMapper.selectAllCustomers(conditionCustomer);
    }

    @Override
    public List<String> queryAllCustomerName(String customerName) {
        return customerMapper.selectAllCustomersName(customerName);
    }

    // 客户列表修改客户信息：根据客户id查询所需要修改的客户信息
    @Override
    public Customer queryCustomerForEditByCustomerId(String customerId) {
        return customerMapper.selectCustomerForEditByCustomerId(customerId);
    }

    // 更新修改之后的客户信息
    @Override
    public int updateEditedCustomer(Customer customer) {
        return customerMapper.updateCustomerByModified(customer);
    }

    // 工作区客户列表页保存新建客户
    @Override
    public int saveNewCustomer(Customer customer, HttpSession session) {
        customer.setId(UUIDUtils.getUUID());
        User user = (User) session.getAttribute(Contants.SESSION_USER);
        customer.setCreateBy(user.getId());
        customer.setCreateTime(DateFormatUtils.DateToString(new Date()));
        return customerMapper.insertNewCustomer(customer);
    }

    // 删除客户信息
    @Override
    @Transactional
    public int deleteCustomersByIds(String[] id, Integer isDeleteContacts) {
        if(isDeleteContacts.equals(Contants.IS_DELETE_CONTACTS_YES)){
            List<String> res = customerContactsRelationMapper.selectContactsIdsByCustomerIds(id); // 获取客户数组对应的联系人数组
            if(!res.isEmpty()){
                String[] contactsIds = res.toArray(new String[] {});
                // 删除联系人对应的备注信息
                contactsRemarkMapper.deleteRemarksByContactsIds(contactsIds);
                // 删除联系人与活动的关联关系 活动联系人关联关系表
                contactsActivityRelationMapper.deleteRelationByContactsIds(contactsIds);
                // 根据客户id数组删除联系人信息
                contactsMapper.deleteContactsByCustomerIds(id);
            }
        }
        // 根据客户数组更改交易中对应的客户信息
        tranMapper.updateAttachByCustomerIds(id);
        // 将客户所对应的联系人的客户id置空
        contactsMapper.updateSetCusNullByCustomerIds(id);
        // 删除客户id删除对应的客户联系人关系
        customerContactsRelationMapper.deleteRelationsByCustomerIds(id);
        // 删除客户id对应的备注信息
        customerRemarkMapper.deleteCustomerRemarksByCustomerIds(id);
        return customerMapper.deleteCustomersByIds(id);
    }
}
