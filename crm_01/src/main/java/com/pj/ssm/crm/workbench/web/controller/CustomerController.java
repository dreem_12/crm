package com.pj.ssm.crm.workbench.web.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pj.ssm.crm.commons.domain.Msg;
import com.pj.ssm.crm.commons.utils.Contants;
import com.pj.ssm.crm.commons.utils.DateFormatUtils;
import com.pj.ssm.crm.settings.domain.User;
import com.pj.ssm.crm.settings.service.UserService;
import com.pj.ssm.crm.workbench.domain.Customer;
import com.pj.ssm.crm.workbench.service.CustomerService;
import java.lang.Exception;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * @Description:
 * @Author: Dreem
 * @Date: 2022 05 15 11:11
 **/
@Controller
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @Autowired
    private UserService userService;

    @RequestMapping("/workbench/customer/customerIndex.do")
    public ModelAndView customerIndex(){
        ModelAndView mv = new ModelAndView();
        List<User> users = userService.queryAllUsers();
        mv.addObject("users", users);
        mv.setViewName("workbench/customer/index");
        return mv;
    }

    @RequestMapping("/workbench/customer/queryAllCustomers.do")
    public @ResponseBody Msg queryAllCustomers(Customer customer, String pn, String ps){
        int pageNo = Integer.parseInt(pn);
        int pageSize = Integer.parseInt(ps);
        // 分页之前，设置去数据库中查询的数量
        PageHelper.startPage(pageNo, pageSize);
        List<Customer> customers = customerService.queryAllCustomers(customer);
        // 分页查询插件，封装分页的各种信息
        PageInfo page = new PageInfo(customers, 5);
        return Msg.success().add("pageInfo", page);
    }

    @RequestMapping("/workbench/customer/queryCustomerForEditCustomer.do")
    public @ResponseBody Msg queryCustomerForEditCustomer(String id){
        try{
            Customer customer = customerService.queryCustomerForEditByCustomerId(id);
            if(customer != null){
                return Msg.success().add("customerForEdit", customer);
            }
            return Msg.fail();
        }catch (Exception e) {
            e.printStackTrace();
            return Msg.fail();
        }
    }

    // 更新修改之后的客户信息
    @RequestMapping("/workbench/customer/updateEditedCustomer.do")
    public @ResponseBody Msg updateEditedCustomer( Customer customer, HttpSession session){
        try{
            User user = (User) session.getAttribute(Contants.SESSION_USER);
            customer.setEditBy(user.getId());
            customer.setEditTime(DateFormatUtils.DateToString(new Date()));
            int count = customerService.updateEditedCustomer(customer);
            if(count > 0){
                return Msg.success();
            }
            return Msg.fail();
        }catch (Exception e) {
            e.printStackTrace();
            return Msg.fail();
        }
    }

    // 保存工作台下客户列表中新创建的客户
    @RequestMapping("/workbench/customer/saveCreateCustomer.do")
    public @ResponseBody Msg saveCreateCustomer(Customer customer, HttpSession session){
        try{
            int count = customerService.saveNewCustomer(customer, session);
            if(count > 0){
                return Msg.success();
            }
            return Msg.fail();
        }catch ( Exception e) {
            e.printStackTrace();
            return Msg.fail();
        }
    }

    // 删除所选的客户信息
    @RequestMapping("/workbench/customer/deleteCustomersSelected.do")
    public @ResponseBody Msg deleteCustomersSelected(String[] id, String isDeleteContacts){
        try{
            Integer flag = Integer.parseInt(isDeleteContacts);
            int count = customerService.deleteCustomersByIds(id, flag);
            if(count > 0){
                return Msg.success().add("count", count);
            }
            return Msg.fail();
        }catch ( Exception e ){
            e.printStackTrace();
            return Msg.fail();
        }
    }
}
