package com.pj.ssm.crm.workbench.web.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.pj.ssm.crm.commons.domain.Msg;
import com.pj.ssm.crm.settings.domain.DicValue;
import com.pj.ssm.crm.settings.domain.User;
import com.pj.ssm.crm.settings.service.DicService;
import com.pj.ssm.crm.settings.service.UserService;
import com.pj.ssm.crm.workbench.domain.Activity;
import com.pj.ssm.crm.workbench.domain.ClueRemark;
import com.pj.ssm.crm.workbench.domain.Contacts;
import com.pj.ssm.crm.workbench.service.ContactsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * @Description: 此处添加描述 //TODO
 * @Author: Dreem
 * @Date: 2022 05 15 13:43
 **/
@Controller
public class ContactsController {

    @Autowired
    private ContactsService contactsService;

    @Autowired
    private UserService userService;

    @Autowired
    private DicService dicService;

    @RequestMapping("/workbench/contacts/contactsIndex.do")
    public ModelAndView contactsIndex(){
        ModelAndView mv = new ModelAndView();
        List<User> users = userService.queryAllUsers();
        List<DicValue> source = dicService.queryDicValueByTypeCode("source");
        List<DicValue> appellation = dicService.queryDicValueByTypeCode("appellation");
        mv.addObject("users", users);
        mv.addObject("source", source);
        mv.addObject("appellation", appellation);
        mv.setViewName("workbench/contacts/index");
        return mv;
    }

    @RequestMapping("/workbench/contacts/queryAllContacts.do")
    public @ResponseBody Msg queryAllContacts(Contacts conditionContact, String pn, String ps){
        int pageNo = Integer.parseInt(pn);
        int pageSize = Integer.parseInt(ps);
        PageHelper.startPage(pageNo, pageSize);
        List<Contacts> contacts = contactsService.queryAllContacts(conditionContact);
        PageInfo page = new PageInfo(contacts, 5);
        return Msg.success().add("pageInfo", page);
    }

    // 保存联系人
    @RequestMapping("/workbench/contacts/saveCreatedContact.do")
    public @ResponseBody Msg saveCreatedContact(Contacts createdContact, HttpSession session){
        try {
            int count = contactsService.saveCreatedContact(createdContact, session);
            if(count > 0){
                return Msg.success().add("count", count);
            }
            return Msg.fail();
        }catch ( Exception e ){
            e.printStackTrace();
            return Msg.fail();
        }
    }

    // 根据id返回联系人具体信息
    @RequestMapping("/workbench/contacts/queryDetailForEditContact.do")
    public @ResponseBody Msg queryDetailForEditContact(String id){
        try {
            Contacts detailForEdit = contactsService.queryDetailByContactId(id);
            if(detailForEdit != null){
                return Msg.success().add("editContact", detailForEdit);
            }
            return Msg.fail();
        }catch ( Exception e ){
            e.printStackTrace();
            return Msg.fail();
        }
    }

    // 更新联系人信息
    @RequestMapping("/workbench/contacts/updateContactByCondition.do")
    public @ResponseBody Msg updateContactByCondition(Contacts contactCondition, HttpSession session){
        try {
            int count = contactsService.updateContactByCondition(contactCondition, session);
            if( count > 0 ){
                return Msg.success().add("count", count);
            }
            return Msg.fail();
        }catch ( Exception e ){
            e.printStackTrace();
            return Msg.fail();
        }
    }

    // 根据id数组删除所选联系人
    @RequestMapping("/workbench/contacts/deleteContactsByIds.do")
    public @ResponseBody Msg deleteContactsByIds(String[] id){
        try {
            String occupationMes = contactsService.judgeOccupation(id);
            if(occupationMes == null){
                int count = contactsService.deleteContactsByContactIds(id);
                if( count > 0 ){
                    return Msg.success().add("count", count);
                }
            }
            return Msg.fail().add("occupationMes", occupationMes);
        }catch ( Exception e ){
            e.printStackTrace();
            return Msg.fail();
        }
    }

    // TODO 根据联系人id获取联系人详细信息
    @RequestMapping("/workbench/contacts/queryDetailByContactId.do")
    public ModelAndView queryDetailByContactId(String id){
        ModelAndView mv = new ModelAndView();
        // 获取联系人明细明细并返回页面
        Contacts contactDetail = contactsService.queryDetailByContactId(id);
        // 获取备注列表
        // List<ClueRemark> clueRemarks = clueRemarkService.queryClueRemarksByClueId(id);
        // 获取线索关联的市场活动
        // List<Activity> relationActivities = activityService.queryRelationActivitiesByClueId(id);
        // mv.addObject("clueDetail", clueDetail);
        // mv.addObject("clueRemarks", clueRemarks);
        // mv.addObject("relationActivities", relationActivities);
        mv.setViewName("workbench/contacts/detail");
        return mv;
    }
}
