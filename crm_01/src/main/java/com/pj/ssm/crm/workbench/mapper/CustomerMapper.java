package com.pj.ssm.crm.workbench.mapper;

import com.pj.ssm.crm.workbench.domain.Customer;

import java.util.List;

public interface CustomerMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_customer
     *
     * @mbggenerated Fri May 13 16:11:07 CST 2022
     */
    int deleteByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_customer
     *
     * @mbggenerated Fri May 13 16:11:07 CST 2022
     */
    int insert(Customer record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_customer
     *
     * @mbggenerated Fri May 13 16:11:07 CST 2022
     */
    int insertSelective(Customer record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_customer
     *
     * @mbggenerated Fri May 13 16:11:07 CST 2022
     */
    Customer selectByPrimaryKey(String id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_customer
     *
     * @mbggenerated Fri May 13 16:11:07 CST 2022
     */
    int updateByPrimaryKeySelective(Customer record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table tbl_customer
     *
     * @mbggenerated Fri May 13 16:11:07 CST 2022
     */
    int updateByPrimaryKey(Customer record);

    /**
     * 插入客户信息
     */
    int insertCustomer(Customer customer);

    /**
     * 客户主页展示所有客户的简要信息
     */
    List<Customer> selectAllCustomers(Customer conditionCustomer);

    /**
     * 根据客名称模糊查询所有匹配的客户
     */
    List<String> selectAllCustomersName(String customerName);

    /**
     * 判断创建交易时是否存在客户信息
     */
    String selectTagByCustomerName(String customerName);

    /**
     * 保存创建交易时创建的新客户
     */
    int insertNewCustomerByCreateNewTran(Customer newCustomer);

    /*
    客户页面修改客户信息：根据id获取客户信息
     */
    Customer selectCustomerForEditByCustomerId(String id);

    /*
    更新修改之后的客户信息
     */
    int updateCustomerByModified(Customer modifiedCustomer);

    /*
    保存创建的客户信息，新建客户保存
     */
    int insertNewCustomer(Customer customer);

    /*
    根据id数组删除所选的客户信息
     */
    int deleteCustomersByIds(String[] id);
}