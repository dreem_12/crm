<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";
%>
<html>
<head>
	<base href="<%=basePath%>">
	<meta charset="UTF-8">

	<link href="jquery/bootstrap_3.3.0/css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
	<link href="jquery/bootstrap-datetimepicker-master/css/bootstrap-datetimepicker.min.css" type="text/css"
		  rel="stylesheet"/>

	<%--	日历插件--%>
	<script type="text/javascript" src="jquery/jquery-1.11.1-min.js"></script>
	<script type="text/javascript" src="jquery/bootstrap_3.3.0/js/bootstrap.min.js"></script>
	<script type="text/javascript"
			src="jquery/bootstrap-datetimepicker-master/js/bootstrap-datetimepicker.min.js"></script>
	<script type="text/javascript"
			src="jquery/bootstrap-datetimepicker-master/locale/bootstrap-datetimepicker.zh-CN.js"></script>

	<%-- pagination 插件--%>
	<link rel="stylesheet" type="text/css" href="jquery/bs_pagination-master/css/jquery.bs_pagination.min.css"/>
	<script type="text/javascript" src="jquery/bs_pagination-master/js/jquery.bs_pagination.min.js"></script>
	<script type="text/javascript" src="jquery/bs_pagination-master/localization/en.js"></script>

	<%-- bs_typeahead自动补全插件 --%>
	<script type="text/javascript" src="jquery/bs_typeahead/bootstrap3-typeahead.min.js"></script>
	<script type="text/javascript">
        function queryAllContactsByCondition(pn, ps) {
            var fullname = $.trim($("#condition-fullname").val());
            var owner = $.trim($("#condition-owner").val());
            var customer = $.trim($("#condition-customer").val());
            var source = $.trim($("#condition-source").val());
            var birth = $.trim($("#condition-birth").val());
            $.ajax({
                url: 'workbench/contacts/queryAllContacts.do',
                type: 'post',
                data: {
                    fullname: fullname,
                    owner: owner,
                    customerId: customer,
                    source: source,
                    birth: birth,
                    pn: pn,
                    ps: ps
                },
                dataType: 'json',
                success: function (data) {
                    if (data.code == '1') {
                        var htmlStr = "";
                        $.each(data.extend.pageInfo.list, function (index, obj) {
                            htmlStr += "<tr>";
                            htmlStr += "<td><input type=\"checkbox\" value=\"" + obj.id + "\"/></td>";
                            htmlStr += "<td><a style=\"text-decoration: none; cursor: pointer;\" onclick=\"window.location.href=''\">" + obj.fullname + obj.appellation + "</a></td>";
                            htmlStr += "<td>" + obj.customerId + "</td>";
                            htmlStr += "<td>" + obj.owner + "</td>";
                            htmlStr += "<td>" + obj.source + "</td>";
                            htmlStr += "<td>" + obj.birth + "</td>";
                            htmlStr += "</tr>";
                        });
                        $("#contactsTBody").html(htmlStr);

                        // 设置全选按钮不选中
                        $("#checkAllBtn").prop("checked", false);

                        $("#contactsNaviBar").bs_pagination({
                            currentPage: pn, // 当前页号，相当于pageNo
                            rowsPerPage: ps, // 每页显示的条数，即pageSize
                            totalRows: data.extend.pageInfo.total, // 总条数
                            totalPages: data.extend.pageInfo.pages, // 总页数，必填参数

                            visiblePageLinks: 5, // 最多可以显示的卡片数
                            showGoToPage: true, // 是否显示"跳转到"部分，默认true--显示
                            showRowsPerPage: true, // 是否显示"每页显示条数"部分，默认true--显示
                            showRowsInfo: true, // 是否显示记录的信息，默认true--显示

                            // 用户每次切换页号，都自动触发页面切换函数，是插件的提供的内部函数
                            // 每次返回切换页号之后的pageNo和pageSize
                            onChangePage: function (event, pageObj) {
                                queryAllContactsByCondition(pageObj.currentPage, pageObj.rowsPerPage);
                            }
                        });
                    } else {
                        alert(data.msg);
                    }
                }
            });
        };

        $(function () {

            queryAllContactsByCondition(1, 5);

            //定制字段
            $("#definedColumns > li").click(function (e) {
                //防止下拉菜单消失
                e.stopPropagation();
            });

            // 为全选按钮绑定单击事件
            $("#checkAllBtn").click(function () {
                $("#contactsTBody input[type='checkbox']").prop("checked", this.checked);
            });

            // 为每一个客户的复选框绑定单击事件
            $("#contactsTBody").on("click", "input[type='checkbox']", function () {
                if ($("#contactsTBody input[type='checkbox']").size() == $("#contactsTBody input[type='checkbox']:checked").size()) $("#checkAllBtn").prop("checked", true);
                else $("#checkAllBtn").prop("checked", false);

            });
            // 为查询按钮绑定单击事件
            $("#queryContactsBtn").click(function () {
                queryAllContactsByCondition(1, $("#contactsNaviBar").bs_pagination('getOption', 'rowsPerPage'));
            });

            // 创建联系人
			$("#createContactBtn").click(function () {
				$("#createContactsModal").modal("show");
            });

			$(".timePick").datetimepicker({
                language:'zh-CN', // 语言
                format:'yyyy-mm-dd', // 日期的格式
                minView:'month', // 可以选择的最小视图
                initialDate:new Date(), // 初始化显示的日期
                autoclose:true, // 设置选择完日期或者时间之后，是否自动关闭日历
                todayBtn:true, // 设置是否显示"今天"按钮，默认设置为false
                clearBtn:true, // 设置是否显示"清空"按钮，默认是false
                pickerPosition:'top-right' // 设置datetimepicker从上面弹出
			});

			// 为客户名称设置自动补全,可优化
			$(".autoAhead").typeahead({
				source: function (jquery, process) {
					$.ajax({
						url: 'workbench/transaction/typeaheadPlugin.do',
						type: 'get',
						data: {
							customerName: jquery
						},
						dataType: 'json',
						success: function (data) {
							process(data.extend.customerNames);
						}
					});
				}
			});

			// 重置表单按钮
			$("#resetCreatedFormBtn").click(function (){
				$("#createContactForm")[0].reset();
			});

			// 保存创建的联系人按钮
			$("#saveCreatedContactBtn").click(function (){
				// 校验必填项
				var fullname = $.trim($("#create-fullname").val());
				if(fullname == null || fullname == ""){
					alert("姓名是必填项...");
					return ;
				}
				var owner = $("#create-owner").val();
				if(owner == null || owner == ""){
					alert("所有者是必填项...");
					return ;
				}
				var appellation = $.trim($("#create-appellation").val());
				var source = $("#create-source").val();
				var job = $.trim($("#create-job").val());
				var mphone = $.trim($("#create-mphone").val());
				var email = $.trim($("#create-email").val());
				var birth = $("#create-birth").val();
				var customerName = $("#create-customerName").val();
				if(customerName == null || customerName == ""){
					alert("客户名称不能为空...");
					return ;
				}
				var description = $.trim($("#create-description").val());
				var contactSummary = $.trim($("#create-contactSummary").val());
				var nextContactTime = $("#create-nextContactTime").val();
				var address = $.trim($("#create-address").val());

				// TODO 判断手机号码是否合法
				// TODO 判断邮箱是否合法
				$.ajax({
					url:'workbench/contacts/saveCreatedContact.do',
					type:'post',
					data:{
						fullname:fullname,
						owner:owner,
						appellation:appellation,
						source:source,
						job:job,
						mphone:mphone,
						email:email,
						birth:birth,
						customerName:customerName,
						description:description,
						contactSummary:contactSummary,
						nextContactTime:nextContactTime,
						address:address
					},
					dataType:'json',
					success:function (data){
						if(data.code == '1'){
							alert("保存成功...");
							queryAllContactsByCondition(1, $("#contactsNaviBar").bs_pagination('getOption', 'rowsPerPage'));
							$("#createContactsModal").modal("hide");
							$("#createContactForm")[0].reset();
						}else{
							alert(data.msg);
						}
					}
				});
			});

			// 修改按钮增加单击事件
			$("#editContactBtn").click(function () {
				var editContact = $("#contactsTBody input[type='checkbox']:checked");
				if (editContact.size() == 0 || editContact.size() > 1) {
					alert("请选择最少一条数据以修改...");
					return;
				}
				var editContactId = editContact[0].value;
				$.ajax({
					url: 'workbench/contacts/queryDetailForEditContact.do',
					type: 'post',
					data: {
						id: editContactId
					},
					dataType: 'json',
					success: function (data) {
						if (data.code == '1') {
							$("#editContactIdHidden").val(editContactId);
							$("#edit-fullname").val(data.extend.editContact.fullname);
							$("#edit-owner").val(data.extend.editContact.owner);
							$("#edit-source").val(data.extend.editContact.source);
							$("#edit-appellation").val(data.extend.editContact.appellation);
							$("#edit-job").val(data.extend.editContact.job);
							$("#edit-mphone").val(data.extend.editContact.mphone);
							$("#edit-email").val(data.extend.editContact.email);
							$("#edit-birth").val(data.extend.editContact.birth);
							$("#edit-customerName").val(data.extend.editContact.customerName);
							$("#edit-description").val(data.extend.editContact.description);
							$("#edit-contactSummary").val(data.extend.editContact.contactSummary);
							$("#edit-nextContactTime").val(data.extend.editContact.nextContactTime);
							$("#edit-address").val(data.extend.editContact.address);
							$("#editContactsModal").modal("show");
						} else {
							alert(data.msg);
						}
					}
				});
			});

			// 为修改中的更新按钮绑定单机事件
			$("#updateContactBtn").click(function () {
				var id = $("#editContactIdHidden").val();
				var fullname = $.trim($("#edit-fullname").val());
				var owner = $("#edit-owner").val();
				var source = $("#edit-source").val();
				var appellation = $("#edit-appellation").val();
				var job = $.trim($("#edit-job").val());
				var mphone = $.trim($("#edit-mphone").val());
				var email = $.trim($("#edit-email").val());
				var birth = $("#edit-birth").val();
				var customerName = $.trim($("#edit-customerName").val());
				var description = $.trim($("#edit-description").val());
				var contactSummary = $.trim($("#edit-contactSummary").val());
				var nextContactTime = $("#edit-nextContactTime").val();
				var address = $.trim($("#edit-address").val());

				// TODO 合法性判断
				if (customerName == null || customerName == '') {
					alert("客户名称不能为空...");
					return;
				}
				var phoneReg = /^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/;
				if (!phoneReg.test(mphone)) {
					alert("请输入正确的电话号码...");
					return;
				}
				// TODO 生日不能超过当前日期
				// TODO 下次联系时间不能小于当前时间

				$.ajax({
					url: 'workbench/contacts/updateContactByCondition.do',
					type: 'post',
					data: {
						id: id,
						fullname: fullname,
						owner: owner,
						source: source,
						appellation: appellation,
						job: job,
						mphone: mphone,
						email: email,
						birth: birth,
						customerName: customerName,
						description: description,
						contactSummary: contactSummary,
						nextContactTime: nextContactTime,
						address: address
					},
					dataType: 'json',
					success: function (data) {
						if (data.code == '1') {
							alert("修改成功...");
							$("#editContactsModal").modal("hide");
						} else {
							alert(data.msg);
						}
					}
				});
			});

			// 为删除绑定单击事件
			$("#deleteContactBtn").click(function () {
				var len = $("#contactsTBody input[type='checkbox']:checked").size();
				if(len == 0){
					alert("请选择最少一条数据以删除...");
					return ;
				}
				$("#deleteContactsH").text("确定要删除这" + len + "条联系人记录？");
				$("#deleteContactsConfirmDialog").modal("show");
			});

			// 确认删除按钮绑定单击事件
			$("#confirmDeleteBtn").click(function (){
				var deletedList = $("#contactsTBody input[type='checkbox']:checked");
				var ids = "";
				$.each(deletedList, function (index, obj){
					ids += "id=" + obj.value + "&";
				});
				ids = ids.substr(0, ids.length - 1);
				$.ajax({
					url:'workbench/contacts/deleteContactsByIds.do',
					type:'post',
					data:ids,
					dataType:'json',
					success:function (data) {
						if(data.code == '1'){
							alert("共有" + data.extend.count + "条记录受影响");
							queryAllContactsByCondition(1, $("#contactsNaviBar").bs_pagination('getOption', 'rowsPerPage'));
							$("#deleteContactsConfirmDialog").modal("hide");
						}else{
							if(data.extend.occupationMes != null){
								alert(data.extend.occupationMes)
							}else{
								alert(data.msg);
							}
						}
					}
				});
			});
        });
	</script>
</head>
<body>

	
	<!-- 创建联系人的模态窗口 -->
	<div class="modal fade" id="createContactsModal" role="dialog">
		<div class="modal-dialog" role="document" style="width: 85%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" onclick="$('#createContactsModal').modal('hide');">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="myModalLabelx">创建联系人</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form" id="createContactForm">
					
						<div class="form-group">
							<label for="create-owner" class="col-sm-2 control-label">所有者<span style="font-size: 15px; color: red;">*</span></label>
							<div class="col-sm-10" style="width: 300px;">
								<select class="form-control" id="create-owner">
								  <c:forEach items="${users}" var="u">
									  <option value="${u.id}">${u.name}</option>
								  </c:forEach>
								</select>
							</div>
							<label for="create-source" class="col-sm-2 control-label">来源</label>
							<div class="col-sm-10" style="width: 300px;">
								<select class="form-control" id="create-source">
								  <option></option>
								  <c:forEach items="${source}" var="s">
									  <option value="${s.id}">${s.value}</option>
								  </c:forEach>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label for="create-fullname" class="col-sm-2 control-label">姓名<span style="font-size: 15px; color: red;">*</span></label>
							<div class="col-sm-10" style="width: 300px;">
								<input type="text" class="form-control" id="create-fullname">
							</div>
							<label for="create-appellation" class="col-sm-2 control-label">称呼</label>
							<div class="col-sm-10" style="width: 300px;">
								<select class="form-control" id="create-appellation">
								  <option></option>
								  <c:forEach items="${appellation}" var="a">
									  <option value="${a.id}">${a.value}</option>
								  </c:forEach>
								</select>
							</div>
							
						</div>
						
						<div class="form-group">
							<label for="create-job" class="col-sm-2 control-label">职位</label>
							<div class="col-sm-10" style="width: 300px;">
								<input type="text" class="form-control" id="create-job">
							</div>
							<label for="create-mphone" class="col-sm-2 control-label">手机</label>
							<div class="col-sm-10" style="width: 300px;">
								<input type="text" class="form-control" id="create-mphone">
							</div>
						</div>
						
						<div class="form-group" style="position: relative;">
							<label for="create-email" class="col-sm-2 control-label">邮箱</label>
							<div class="col-sm-10" style="width: 300px;">
								<input type="text" class="form-control" id="create-email">
							</div>
							<label for="create-birth" class="col-sm-2 control-label">生日</label>
							<div class="col-sm-10" style="width: 300px;">
								<input type="text" class="form-control timePick" id="create-birth" readonly>
							</div>
						</div>
						
						<div class="form-group" style="position: relative;">
							<label for="create-customerName" class="col-sm-2 control-label">客户名称<span style="font-size: 15px; color: red;">*</span></label>
							<div class="col-sm-10" style="width: 300px;">
								<input type="text" class="form-control autoAhead" id="create-customerName" placeholder="支持自动补全，输入客户不存在则新建">
							</div>
						</div>
						
						<div class="form-group" style="position: relative;">
							<label for="create-description" class="col-sm-2 control-label">描述</label>
							<div class="col-sm-10" style="width: 81%;">
								<textarea class="form-control" rows="3" id="create-description"></textarea>
							</div>
						</div>
						
						<div style="height: 1px; width: 103%; background-color: #D5D5D5; left: -13px; position: relative;"></div>
						
						<div style="position: relative;top: 15px;">
							<div class="form-group">
								<label for="create-contactSummary" class="col-sm-2 control-label">联系纪要</label>
								<div class="col-sm-10" style="width: 81%;">
									<textarea class="form-control" rows="3" id="create-contactSummary"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="create-nextContactTime" class="col-sm-2 control-label">下次联系时间</label>
								<div class="col-sm-10" style="width: 300px;">
									<input type="text" class="form-control timePick" id="create-nextContactTime" readonly>
								</div>
							</div>
						</div>

                        <div style="height: 1px; width: 103%; background-color: #D5D5D5; left: -13px; position: relative; top : 10px;"></div>

                        <div style="position: relative;top: 20px;">
                            <div class="form-group">
                                <label for="create-address" class="col-sm-2 control-label">详细地址</label>
                                <div class="col-sm-10" style="width: 81%;">
                                    <textarea class="form-control" rows="1" id="create-address"></textarea>
                                </div>
                            </div>
                        </div>
					</form>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" id="resetCreatedFormBtn">重置表单</button>
					<button type="button" class="btn btn-primary" id="saveCreatedContactBtn">保存</button>
				</div>
			</div>
		</div>
	</div>
	
	<!-- 修改联系人的模态窗口 -->
	<div class="modal fade" id="editContactsModal" role="dialog">
		<div class="modal-dialog" role="document" style="width: 85%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title" id="myModalLabel1">修改联系人</h4>
				</div>
				<div class="modal-body">
					<form class="form-horizontal" role="form">
						<input type="hidden" id="editContactIdHidden">
						<div class="form-group">
							<label for="edit-owner" class="col-sm-2 control-label">所有者<span style="font-size: 15px; color: red;">*</span></label>
							<div class="col-sm-10" style="width: 300px;">
								<select class="form-control" id="edit-owner">
									<c:forEach items="${users}" var="u">
										<option value="${u.id}">${u.name}</option>
									</c:forEach>
								</select>
							</div>
							<label for="edit-source" class="col-sm-2 control-label">来源</label>
							<div class="col-sm-10" style="width: 300px;">
								<select class="form-control" id="edit-source">
								  <option></option>
									<c:forEach items="${source}" var="s">
										<option value="${s.id}">${s.value}</option>
									</c:forEach>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label for="edit-fullname" class="col-sm-2 control-label">姓名<span style="font-size: 15px; color: red;">*</span></label>
							<div class="col-sm-10" style="width: 300px;">
								<input type="text" class="form-control" id="edit-fullname">
							</div>
							<label for="edit-appellation" class="col-sm-2 control-label">称呼</label>
							<div class="col-sm-10" style="width: 300px;">
								<select class="form-control" id="edit-appellation">
								  <option></option>
								  <c:forEach items="${appellation}" var="a">
									  <option value="${a.id}">${a.value}</option>
								  </c:forEach>
								</select>
							</div>
						</div>
						
						<div class="form-group">
							<label for="edit-job" class="col-sm-2 control-label">职位</label>
							<div class="col-sm-10" style="width: 300px;">
								<input type="text" class="form-control" id="edit-job" value="CTO">
							</div>
							<label for="edit-mphone" class="col-sm-2 control-label">手机</label>
							<div class="col-sm-10" style="width: 300px;">
								<input type="text" class="form-control" id="edit-mphone">
							</div>
						</div>
						
						<div class="form-group">
							<label for="edit-email" class="col-sm-2 control-label">邮箱</label>
							<div class="col-sm-10" style="width: 300px;">
								<input type="text" class="form-control" id="edit-email">
							</div>
							<label for="edit-birth" class="col-sm-2 control-label">生日</label>
							<div class="col-sm-10" style="width: 300px;">
								<input type="text" class="form-control timePick" readonly id="edit-birth">
							</div>
						</div>
						
						<div class="form-group">
							<label for="edit-customerName" class="col-sm-2 control-label">客户名称<span style="font-size: 15px; color: red;">*</span></label>
							<div class="col-sm-10" style="width: 300px;">
								<input type="text" class="form-control autoAhead" id="edit-customerName" placeholder="支持自动补全，输入客户不存在则新建">
							</div>
						</div>
						
						<div class="form-group">
							<label for="edit-description" class="col-sm-2 control-label">描述</label>
							<div class="col-sm-10" style="width: 81%;">
								<textarea class="form-control" rows="3" id="edit-description" maxlength="255"></textarea>
							</div>
						</div>
						
						<div style="height: 1px; width: 103%; background-color: #D5D5D5; left: -13px; position: relative;"></div>
						
						<div style="position: relative;top: 15px;">
							<div class="form-group">
								<label for="create-contactSummary" class="col-sm-2 control-label">联系纪要</label>
								<div class="col-sm-10" style="width: 81%;">
									<textarea class="form-control" rows="3" id="edit-contactSummary" maxlength="255"></textarea>
								</div>
							</div>
							<div class="form-group">
								<label for="create-nextContactTime" class="col-sm-2 control-label">下次联系时间</label>
								<div class="col-sm-10" style="width: 300px;">
									<input type="text" class="form-control timePick" readonly id="edit-nextContactTime">
								</div>
							</div>
						</div>
						
						<div style="height: 1px; width: 103%; background-color: #D5D5D5; left: -13px; position: relative; top : 10px;"></div>

                        <div style="position: relative;top: 20px;">
                            <div class="form-group">
                                <label for="edit-address" class="col-sm-2 control-label">详细地址</label>
                                <div class="col-sm-10" style="width: 81%;">
                                    <textarea class="form-control" rows="1" id="edit-address" maxlength="100"></textarea>
                                </div>
                            </div>
                        </div>
					</form>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
					<button type="button" class="btn btn-primary" id="updateContactBtn">更新</button>
				</div>
			</div>
		</div>
	</div>

	<%--确认删除的模态框--%>
	<div class="modal fade" id="deleteContactsConfirmDialog" role="dialog">
		<div class="modal-dialog" role="document" style="width: 40%;">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">
						<span aria-hidden="true">×</span>
					</button>
					<h4 class="modal-title"><span style="font-size: 30px; color: red;">*</span><span style="font-size: 30px; color: greenyellow;">*</span><span style="font-size: 30px; color: yellow;">*</span></h4>
				</div>
				<div class="modal-body">
					<h4  id="deleteContactsH"></h4>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
					<button type="button" class="btn btn-primary" id="confirmDeleteBtn">确定</button>
				</div>
			</div>
		</div>
	</div>

	<div>
		<div style="position: relative; left: 10px; top: -10px;">
			<div class="page-header">
				<h3>联系人列表</h3>
			</div>
		</div>
	</div>
	
	<div style="position: relative; top: -20px; left: 0px; width: 100%; height: 100%;">
	
		<div style="width: 100%; position: absolute;top: 5px; left: 10px;">
		
			<div class="btn-toolbar" role="toolbar" style="height: 80px;">
				<form class="form-inline" role="form" style="position: relative;top: 8%; left: 5px;">
				  
				  <div class="form-group">
				    <div class="input-group">
				      <div class="input-group-addon">所有者</div>
				      <input class="form-control" type="text" id="condition-owner">
				    </div>
				  </div>
				  
				  <div class="form-group">
				    <div class="input-group">
				      <div class="input-group-addon">姓名</div>
				      <input class="form-control" type="text" id="condition-fullname">
				    </div>
				  </div>
				  
				  <div class="form-group">
				    <div class="input-group">
				      <div class="input-group-addon">客户名称</div>
				      <input class="form-control" type="text" id="condition-customer">
				    </div>
				  </div>
				  
				  <br>
				  
				  <div class="form-group">
				    <div class="input-group">
				      <div class="input-group-addon">来源</div>
				      <select class="form-control" id="condition-source">
						  <option></option>
						  <c:forEach items="${source}" var="s">
							  <option value="${s.id}">${s.value}</option>
						  </c:forEach>
						</select>
				    </div>
				  </div>
				  
				  <div class="form-group">
				    <div class="input-group">
				      <div class="input-group-addon">生日</div>
				      <input class="form-control" type="text" id="condition-birth">
				    </div>
				  </div>
				  
				  <button id="queryContactsBtn" type="button" class="btn btn-default">查询</button>
				  
				</form>
			</div>
			<div class="btn-toolbar" role="toolbar" style="background-color: #F7F7F7; height: 50px; position: relative;top: 10px;">
				<div class="btn-group" style="position: relative; top: 18%;">
				  <button type="button" class="btn btn-primary" id="createContactBtn"><span class="glyphicon glyphicon-plus"></span> 创建</button>
				  <button type="button" class="btn btn-default" id="editContactBtn"><span class="glyphicon glyphicon-pencil"></span> 修改</button>
				  <button type="button" class="btn btn-danger" id="deleteContactBtn"><span class="glyphicon glyphicon-minus"></span> 删除</button>
				</div>
				
				
			</div>
			<div style="position: relative;top: 20px;">
				<table class="table table-hover">
					<thead>
						<tr style="color: #B3B3B3;">
							<td><input type="checkbox" id="checkAllBtn"/></td>
							<td>姓名</td>
							<td>客户名称</td>
							<td>所有者</td>
							<td>来源</td>
							<td>生日</td>
						</tr>
					</thead>
					<tbody id="contactsTBody">
					</tbody>
				</table>
				<div id="contactsNaviBar"></div>
			</div>
		</div>
		
	</div>
</body>
</html>