# 练手项目crm

#### 介绍
学习SSM框架的第一个SSM练手项目，Spring+SpringMVC+Mybatis  jQuery+Bootstarp+ajax，这个项目是动力节点的一个CRM项目。
前端页面使用到的插件有Bootstrap的日历插件和分页插件，图标使用的是Echarts插件。
后端操作excel、word使用的是poi，分页插件使用的是pagehelper。
#### 注意
1. 因为项目的注册功能暂时没开发，所以需要自己通过数据库管理软件在tbl_user表中添加一个用户以用来登录
2. 项目暂时没有增加权限这一项，所以只需要在数据库中设置用户登录即可
3. crm.sql 是只有结构的，crm2.sql 是带一点数据的，供参考
#### 使用说明

1. 在src目录中的resource目录下有一个possibility.properties和jdbc.properties
    possibility.properties设置交易阶段成交的概率，根据自身需求修改即可
    jdbc.properties 设置连接数据库的账号和密码等，根据自身系统做修改
2. 另一个模块mybatis-generator是用来逆向生成代码的，可以不用管
3. 直接克隆下来，选择maven项目，导入即可 
4. 设置tomcat服务器的时候需要设置如下图：
![img.png](img.png)