package com.pj.ssm.crm.workbench.service.impl;

import com.pj.ssm.crm.commons.utils.Contants;
import com.pj.ssm.crm.commons.utils.DateFormatUtils;
import com.pj.ssm.crm.commons.utils.UUIDUtils;
import com.pj.ssm.crm.settings.domain.User;
import com.pj.ssm.crm.workbench.domain.Contacts;
import com.pj.ssm.crm.workbench.domain.Customer;
import com.pj.ssm.crm.workbench.domain.CustomerContactsRelation;
import com.pj.ssm.crm.workbench.mapper.ContactsMapper;
import com.pj.ssm.crm.workbench.mapper.CustomerContactsRelationMapper;
import com.pj.ssm.crm.workbench.mapper.CustomerMapper;
import com.pj.ssm.crm.workbench.mapper.TranMapper;
import com.pj.ssm.crm.workbench.service.ContactsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;

/**
 * @Description: 此处添加描述 //TODO
 * @Author: Dreem
 * @Date: 2022 05 15 13:12
 **/
@Service("contactsService")
public class ContactsServiceImpl implements ContactsService {

    @Resource
    private ContactsMapper contactsMapper;

    @Resource
    private TranMapper tranMapper;

    @Resource
    private CustomerMapper customerMapper;

    @Resource
    private CustomerContactsRelationMapper customerContactsRelationMapper;

    @Override
    public List<Contacts> queryAllContacts(Contacts conditionContact) {
        return contactsMapper.selectAllContacts(conditionContact);
    }

    @Override
    public List<Contacts> queryContactsByContactName(String contactName) {
        return contactsMapper.selectContactsByContactName(contactName);
    }

    @Override
    @Transactional
    public int saveCreatedContact(Contacts createdContact, HttpSession session) {
        User user = (User) session.getAttribute(Contants.SESSION_USER);
        createdContact.setId(UUIDUtils.getUUID());
        createdContact.setCreateBy(user.getId());
        createdContact.setCreateTime(DateFormatUtils.DateToString(new Date()));
        String customerId = getNewCustomerId(createdContact, user);
        createdContact.setCustomerId(customerId);
        return contactsMapper.insertCreatedContact(createdContact);
    }

    // 根据id查询联系人的详细信息
    @Override
    public Contacts queryDetailByContactId(String contactId) {
        return contactsMapper.selectDetailByContactId(contactId);
    }

    @Override
    @Transactional
    public int updateContactByCondition(Contacts contactCondition, HttpSession session) {
        User user = (User) session.getAttribute(Contants.SESSION_USER);
        contactCondition.setEditBy(user.getId());
        contactCondition.setEditTime(DateFormatUtils.DateToString(new Date()));
        // 获取联系人对应的客户id信息
        String customerId = getNewCustomerId(contactCondition, user);
        contactCondition.setCustomerId(customerId);
        return contactsMapper.updateContactByCondition(contactCondition);
    }

    @Override
    public int deleteContactsByContactIds(String[] ids) {
        customerContactsRelationMapper.deleteRelationsByContactIds(ids);
        return contactsMapper.deleteContactsByContactIds(ids);
    }

    private String getNewCustomerId(Contacts contacts, User user){
        String relationCustomerId = customerContactsRelationMapper.selectCustomerIdByContactId(contacts.getId());
        String customerId = customerMapper.selectTagByCustomerName(contacts.getCustomerName());
        if(relationCustomerId != null && !relationCustomerId.equals(customerId) && customerId != null){
            customerContactsRelationMapper.updateRelationByContactId(contacts.getId(), customerId);
            return customerId;
        }
        if(customerId == null){
            Customer newCustomer = new Customer();
            newCustomer.setId(UUIDUtils.getUUID());
            newCustomer.setName(contacts.getCustomerName());
            newCustomer.setOwner(contacts.getOwner());
            newCustomer.setCreateBy(user.getId());
            newCustomer.setCreateTime(DateFormatUtils.DateToString(new Date()));
            newCustomer.setContactSummary(contacts.getContactSummary());
            newCustomer.setNextContactTime(contacts.getNextContactTime());
            if(contacts.getEditBy() != null){
                newCustomer.setDescription("修改联系人:" + contacts.getFullname() + "时新建的客户...");
            }else{
                newCustomer.setDescription("新建联系人:" + contacts.getFullname() + "时新建的客户...");
            }
            customerId = newCustomer.getId();
            customerMapper.insertNewCustomer(newCustomer);
        }
        if(relationCustomerId == null){
            // 向联系人客户表中增加新增信息
            CustomerContactsRelation customerContactsRelation = new CustomerContactsRelation();
            customerContactsRelation.setId(UUIDUtils.getUUID());
            customerContactsRelation.setCustomerId(customerId);
            customerContactsRelation.setContactsId(contacts.getId());
            customerContactsRelationMapper.insert(customerContactsRelation);
        }
        return customerId;
    }

    @Override
    public String judgeOccupation(String[] ids){
        String message = null;
        String contactName = null;
        for(String id : ids){
            contactName = tranMapper.selectTranIdByContactId(id);
            if(contactName == null){
                message += "联系人\"" + contactName + "\"在交易表中有交易信息，请在交易表中解除占用...\n";
            }
        }
        return message;
    }
}
