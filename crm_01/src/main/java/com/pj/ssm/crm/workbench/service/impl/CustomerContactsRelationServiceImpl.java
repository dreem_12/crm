package com.pj.ssm.crm.workbench.service.impl;

import com.pj.ssm.crm.workbench.mapper.CustomerContactsRelationMapper;
import com.pj.ssm.crm.workbench.service.CustomerContactsRelationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Description: 此处添加描述 //TODO
 * @Author: Dreem
 * @Date: 2022 05 09 17:23
 **/
@Service("customerContactsRelationService")
public class CustomerContactsRelationServiceImpl implements CustomerContactsRelationService {

    @Autowired
    private CustomerContactsRelationMapper customerContactsRelationMapper;

    @Override
    public int deleteRelationsByCustomerIds(String[] id) {
        return customerContactsRelationMapper.deleteRelationsByCustomerIds(id);
    }
}
